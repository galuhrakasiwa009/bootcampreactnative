function arrayToObject(arr) {
    // Code di sini
    if(arr.length == 0){
      console.log('')
    }
    var now = new Date()
    var thisYear = now.getFullYear()
    for(let i = 0; i < arr.length; i++){
      let obj = {
        "firstName" : `${arr[i][0]}`,
        "firstName" : `${arr[i][1]}`,
        'gender' : `${arr[i][2]}`,
        'age' : `${thisYear}-${arr[i][3]}`
      }
      console.log(`${i+1}. ${arr[i][0]} ${arr[i][1]}: `,obj)
    }
  }
  
  // Driver Code
  var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
  arrayToObject(people) 
  /*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
  */
  
  var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
  arrayToObject(people2) 
  /*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
  */
  
  // Error case 
  arrayToObject([]) // ""




  // 2

  function shoppingTime(memberId, money) {
    // you can only write your code here!
    if(!memberId){
      return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    }
    
    let listItems = [
      {'item': 'Sepatu brand Stacattu', 'harga': 1500000},
      {'item': 'Baju brand Zoro', 'harga': 500000},
      {'item': 'Baju brand H&N', 'harga': 250000},
      {'item': 'Sweater brand Uniklooh', 'harga': 175000},  
      {'item': 'Casing Handphone', 'harga': 50000}
    ]
    let uangSisa = money;
    let purchasedItem = []
    for(let i = 0; i < listItems.length; i++){
      if(uangSisa - listItems[i].harga >= 0){
        uangSisa = uangSisa - listItems[i].harga
        purchasedItem.push(listItems[i].item)
      } else {
        continue
      }
    }
    if(purchasedItem.length == 0){
      return 'Mohon maaf, uang tidak cukup'
    }
    return {
      memberId: memberId,
      money: money,
      listPurchased: purchasedItem,
      changeMoney: uangSisa
    }
  
  }
  
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


  // 3


  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    let result = []
    for(let i = 0; i < arrPenumpang.length; i++){
      result.push({
        penumpang:`${arrPenumpang[i][0]}`,
        naikDari: `${arrPenumpang[i][1]}`,
        tujuan: `${arrPenumpang[i][2]}`,
        bayar: (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1])) * 2000
      })
    }
    return result
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]