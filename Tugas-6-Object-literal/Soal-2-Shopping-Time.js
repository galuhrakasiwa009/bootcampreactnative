function shoppingTime(memberId, money) {
  // you can only write your code here!
  if(!memberId){
    return 'Mohon maaf, toko X hanya berlaku untuk member saja'
  }
  
  let listItems = [
    {'item': 'Sepatu brand Stacattu', 'harga': 1500000},
    {'item': 'Baju brand Zoro', 'harga': 500000},
    {'item': 'Baju brand H&N', 'harga': 250000},
    {'item': 'Sweater brand Uniklooh', 'harga': 175000},  
    {'item': 'Casing Handphone', 'harga': 50000}
  ]
  let uangSisa = money;
  let purchasedItem = []
  for(let i = 0; i < listItems.length; i++){
    if(uangSisa - listItems[i].harga >= 0){
      uangSisa = uangSisa - listItems[i].harga
      purchasedItem.push(listItems[i].item)
    } else {
      continue
    }
  }
  if(purchasedItem.length == 0){
    return 'Mohon maaf, uang tidak cukup'
  }
  return {
    memberId: memberId,
    money: money,
    listPurchased: purchasedItem,
    changeMoney: uangSisa
  }

}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja