// function Clock({ template }) {
  
//     var timer;
  
//     function render() {
//       var date = new Date();
  
//       var hours = date.getHours();
//       if (hours < 10) hours = '0' + hours;
  
//       var mins = date.getMinutes();
//       if (mins < 10) mins = '0' + mins;
  
//       var secs = date.getSeconds();
//       if (secs < 10) secs = '0' + secs;
  
//       var output = template
//         .replace('h', hours)
//         .replace('m', mins)
//         .replace('s', secs);
  
//       console.log(output);
//     }
  
//     this.stop = function() {
//       clearInterval(timer);
//     };
  
//     this.start = function() {
//       render();
//       timer = setInterval(render, 1000);
//     };
  
//   }
  
//   var clock = new Clock({template: 'h:m:s'});
//   clock.start(); 
  
//   function di atas diubah menjadi struktur class seperti berikut:
class Clock {
  constructor({
    template
  }) {
    this.template = template
    this.timer
  }
  render() {
    var date = new Date();
    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;
    var minutes = date.getMinutes();
    if (minutes < 10) minutes = '0' + minutes;
    var seconds = date.getSeconds();
    if (seconds < 10) seconds = '0' + seconds;
    var output = this.template
      .replace('h', hours)
      .replace('m', minutes)
      .replace('s', seconds);

    console.log(output)
  }
  start() {
    this.render()
    this.timer = setInterval(()=>{this.render()}, 1000);
  }
  stop() {
    clearInterval(this.timer);
  }
}
var clock = new Clock({template: 'h:m:s'});
clock.start();