
// Release 0

class Animal {
    // Code class di sini
    constructor(name){
        this.name = name,
        this.legs = 4,
        this.cold_blooded = false
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false




// Release 1

// Code class Ape dan class Frog di sini

class Ape extends Animal {
    constructor (name){
        super (name),
        this.legs = 2,
        this.cold_blooded = false
    }
    yell() {
        console.log('auooo');
        console.log(this.name);
        console.log(this.legs);
        console.log(this.cold_blooded);
        
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"


 
class Frog extends Animal {
    constructor (name){
        super (name),
        this.legs = 4,
        this.cold_blooded = false
    }
    jump() {
        console.log('hop hop');
        console.log(this.name);
        console.log(this.legs);
        console.log(this.cold_blooded);
        
    }
}


var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

