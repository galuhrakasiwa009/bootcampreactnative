const rangeWithStep = function(num1, num2, step){
    if(!num1 || !num2){
        return -1;
    }
    let result = [];
    if(num1 < num2){
        for(let i = num1; i <= num2 ; i+=step){
            result.push(i);
        }
    }
    else {
        for(let i = num1; i >= num2 ; i-=step){
            result.push(i);
        }
    }
    return result;
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]