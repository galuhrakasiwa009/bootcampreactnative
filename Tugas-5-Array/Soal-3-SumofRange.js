const sum = function(num1, num2, step){
    if(!num1){
        return 0;
    }
    if(!num2){
        return num1;
    }
    if(!step){
        step = 1;
    }
    let result = 0;
    if(num1 < num2){
        for(let i = num1; i <= num2 ; i+=step){
            result+= i;
        }
    }
    else {
        for(let i = num1; i >= num2 ; i-=step){
            result+= i;
        }
    }
    return result;
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0