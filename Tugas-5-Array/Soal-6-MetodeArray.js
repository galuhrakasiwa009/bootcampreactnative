const month = function (input) {
    switch (input) {
        case "01":
            return "Januari";
            break;
        case "02":
            return "Februari";
            break;
        case "03":
            return "Maret";
            break;
        case "04":
            return "April";
            break;
        case "05":
            return "Mei";
            break;
        case "06":
            return "Juni";
            break;
        case "07":
            return "Juli";
            break;
        case "08":
            return "Agustus";
            break;
        case "09":
            return "September";
            break;
        case "10":
            return "Oktober";
            break;
        case "11":
            return "November";
            break;
        case "12":
            return "Desember";
            break;
    }
};
const dataHandling2 = function (input) {
    console.log(input);
    let dateOfBirth = input[3].split("/");
    let monthString = month(dateOfBirth[1]);
    console.log(monthString);
    console.log(
        dateOfBirth
            .map((datum) => {
                return parseInt(datum);
            })
            .sort()
            .map((datum) => {
                return datum.toString();
            })
    );
    console.log(input[3].split("/").join("-"));
    let splicedName = input[1];
    if (input[1].length > 15) {
        splicedName = input[1].slice(0, 15);
    }
    console.log(splicedName);
};
var input = [
    "0001",
    "Roman Alamsyah ",
    "Bandar Lampung",
    "21/05/1989",
    "Membaca",
];
dataHandling2(input);