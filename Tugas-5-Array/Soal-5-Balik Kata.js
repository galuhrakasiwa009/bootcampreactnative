const balikKata = function(input){
    let result = "";
    let temp = ""
    for(let i = input.length - 1; i >= 0; i--){
        temp = temp + input[i]
        result = temp;
    }
    return result;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I