// // no 1

// const range = function(startNum, finishNum){
//     if(!startNum || !finishNum){
//         return -1;
//     }
//     let result = [];
//     let start, end, increment;
//     if(startNum < finishNum){
//         start = startNum;
//         end = finishNum+1;
//     }
//     else {
//         start = finishNum;
//         end = startNum+1;
//     }
//     // console.log(start)
//     for(let i = start; i < end ; i++){
//         result.push(i);
//     }
//     if(finishNum < startNum){
//      result.reverse()   
//     }
//     return result;
// }
// console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
// console.log(range(1)) // -1
// console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
// console.log(range(54, 50)) // [54, 53, 52, 51, 50]
// console.log(range()) // -1


// // No2

// const rangeWithStep = function(num1, num2, step){
//     if(!num1 || !num2){
//         return -1;
//     }
//     let result = [];
//     if(num1 < num2){
//         for(let i = num1; i <= num2 ; i+=step){
//             result.push(i);
//         }
//     }
//     else {
//         for(let i = num1; i >= num2 ; i-=step){
//             result.push(i);
//         }
//     }
//     return result;
// }
// console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
// console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
// console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
// console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

// //3

// const sum = function(num1, num2, step){
//     if(!num1){
//         return 0;
//     }
//     if(!num2){
//         return num1;
//     }
//     if(!step){
//         step = 1;
//     }
//     let result = 0;
//     if(num1 < num2){
//         for(let i = num1; i <= num2 ; i+=step){
//             result+= i;
//         }
//     }
//     else {
//         for(let i = num1; i >= num2 ; i-=step){
//             result+= i;
//         }
//     }
//     return result;
// }
// console.log(sum(1,10)) // 55
// console.log(sum(5, 50, 2)) // 621
// console.log(sum(15,10)) // 75
// console.log(sum(20, 10, 2)) // 90
// console.log(sum(1)) // 1
// console.log(sum()) // 0

// // no 4


// const input = [
//     ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
//     ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
//     ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
//     ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
// ] 
// for(let i = 0; i < input.length; i++){
//     console.log("Nomor ID \t: " + input[i][0]);
//     console.log("Nama Lengkap\t: " + input[i][1]);
//     console.log("TTL \t\t: " + input[i][2]);
//     console.log("Hobi \t\t: " + input[i][3]);
//     console.log("\n")
// }


// // no 5

// const balikKata = function(input){
//     let result = "";
//     let temp = ""
//     for(let i = input.length - 1; i >= 0; i--){
//         temp = temp + input[i]
//         result = temp;
//     }
//     return result;
// }
// console.log(balikKata("Kasur Rusak")) // kasuR rusaK
// console.log(balikKata("SanberCode")) // edoCrebnaS
// console.log(balikKata("Haji Ijah")) // hajI ijaH
// console.log(balikKata("racecar")) // racecar
// console.log(balikKata("I am Sanbers")) // srebnaS ma I



// // no 6


// const month = function (input) {
//     switch (input) {
//         case "01":
//             return "Januari";
//             break;
//         case "02":
//             return "Februari";
//             break;
//         case "03":
//             return "Maret";
//             break;
//         case "04":
//             return "April";
//             break;
//         case "05":
//             return "Mei";
//             break;
//         case "06":
//             return "Juni";
//             break;
//         case "07":
//             return "Juli";
//             break;
//         case "08":
//             return "Agustus";
//             break;
//         case "09":
//             return "September";
//             break;
//         case "10":
//             return "Oktober";
//             break;
//         case "11":
//             return "November";
//             break;
//         case "12":
//             return "Desember";
//             break;
//     }
// };
// const dataHandling2 = function (input) {
//     console.log(input);
//     let dateOfBirth = input[3].split("/");
//     let monthString = month(dateOfBirth[1]);
//     console.log(monthString);
//     console.log(
//         dateOfBirth
//             .map((datum) => {
//                 return parseInt(datum);
//             })
//             .sort()
//             .map((datum) => {
//                 return datum.toString();
//             })
//     );
//     console.log(input[3].split("/").join("-"));
//     let splicedName = input[1];
//     if (input[1].length > 15) {
//         splicedName = input[1].slice(0, 15);
//     }
//     console.log(splicedName);
// };
// var input = [
//     "0001",
//     "Roman Alamsyah ",
//     "Bandar Lampung",
//     "21/05/1989",
//     "Membaca",
// ];
// dataHandling2(input);