const range = function(startNum, finishNum){
    if(!startNum || !finishNum){
        return -1;
    }
    let result = [];
    let start, end, increment;
    if(startNum < finishNum){
        start = startNum;
        end = finishNum+1;
    }
    else {
        start = finishNum;
        end = startNum+1;
    }
    // console.log(start)
    for(let i = start; i < end ; i++){
        result.push(i);
    }
    if(finishNum < startNum){
     result.reverse()   
    }
    return result;
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1