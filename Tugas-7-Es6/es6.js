//1

const golden =  (goldenFunction => console.log("this is golden!!"))
   
  golden()


  //2
  const firstName = "William";
const lastName = "Imoh";

let fullName = (firstName, lastName) => {
  return `${firstName} ${lastName};`
}
console.log(fullName(firstName, lastName));


//3

const newObject = {
    firstName1: "Harry",
    lastName1: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  
  const {firstName1, lastName1, destination, occupation, spell} = newObject
  console.log(firstName1, lastName1, destination, occupation,"!!!")
  

  //4
  const west = ["Will", "Chris", "Sam", "Holly"]
  const east = ["Gill", "Brian", "Noel", "Maggie"]
  
  let combined = [...west, ...east]
  console.log(combined)

  //5

  const planet = "earth"
const view = "glass"
var before = `Lorem  ${view} dolor sit amet,   
    consectetur adipiscing elit, ${planet} do eiusmod tempor  +
    incididunt ut labore et dolore magna aliqua. Ut enim +
     ad minim veniam`
 
// Driver Code
console.log(before) 

