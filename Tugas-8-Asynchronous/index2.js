
let readBooksPromise = require('./promise.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

async function Book(){
    let time = 10000
    for (let i = 0; i < books.length; i++){
        time = await readBooksPromise(time, books[i]).then(function(sisaWaktu){
            return sisaWaktu
        })
    .catch(function(sisaWaktu){
        return sisaWaktu
    })
    
    }

    console.log('selesai');

}

Book()
