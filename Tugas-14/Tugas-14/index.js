import React, { useState, useEffect } from "react";
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  Touchable,
  TouchableOpacity,
  View,
  Image,
  TextInput,
  Button,
} from "react-native";
import axios from "axios";

export default function Tugas14() {
  const [title, setTitle] = useState("");
  const [value, setValue] = useState("");
  const [item, setItem] = useState([]);
  const [button, setButton] = useState("Simpan");
  const [selectedUser, setSelectedUser] = useState({});

  const submit = () => {
    const data = {
      value,
      title,
    };

    if (button == "Simpan") {
      axios
        .post("https://achmadhilmy-sanbercode.my.id/api/v1/news", data)
        .then((res) => {
          console.log("res: ", res);
          setTitle("");
          setValue("");
          GetData();
        })
        .catch((err) => {
          console.log("error: ", err);
        });
    } else {
      axios
        .put("https://achmadhilmy-sanbercode.my.id/api/v1/news/{id_news}", data)
        .then((res) => {
          console.log("res: ", res);
          setTitle("");
          setValue("");
          GetData();
          setButton("Simpan");
        })
        .catch((err) => {
          console.log("error: ", err);
        });
    }
  };
  const onSelectItem = (item) => {
    console.log(item);
    setSelectedUser(item);
    setTitle(item.title);
    setValue(item.value);
    setButton("Update");
  };
  const GetData = () => {
    axios
      .get("https://achmadhilmy-sanbercode.my.id/api/v1/news")
      .then((res) => {
        const data1 = res.data.data;
        console.log(("res: ", data1));
        setItem(data1);
      });
  };
  const onDelete = (item) => {
    axios
      .delete(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${item.id}`)
      .then((res) => {
        console.log("res: ", res);
        GetData();
      })
      .catch((err) => {
        console.log("error", err);
      });
  };
  useEffect(() => {
    GetData();
  }, []);
  return (
    <View style={styles.containr}>
      <View style={styles.header}>
        <Text style={styles.title}> Tampilkan Api (CRUD)</Text>
      </View>
      <View style={styles.content1}>
        <Text>Post Data</Text>
        <TextInput
          placeholder="masukan judul berita" 
          style={styles.input}
          value={title}
          oneChangeText={(value) => setTitle(value)}
        />
        <TextInput
          placeholder="masukan isi berita"
          style={styles.input}
          value={value}
          oneChangeText={(value) => setValue(value)}
        />
        <Button title={button} onPress={submit} />
      </View>
      <View style={styles.content1}>
        <Text>get Data Berita</Text>

        <FlatList
          data={item}
          keyExtractor={(item, index) => `${item.id}-${index}`}
          renderItem={({ item }) => {
            return (
              <View>
                <TouchableOpacity onPress={() => onDelete(item)}>
                  <Text style={{ color: "red", alignSelf: "flex-end" }}>X</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => onSelectItem(item)}
                  style={{ borderButtonColor: "red" }}
                >
                  <Text style={{ color: "white" }}> {item.title}</Text>
                  <Text style={{ color: "white" }}> {item.value}</Text>
                </TouchableOpacity>
              </View>
            );
          }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  header: {
    paddingTop: 50,
    paddingHorizontal: 16,
    backgroundColor: "grey",
    alignItems: "center",
  },
  title: {
    color: "white",
    fontSize: 20,
  },
  content1: {
    paddingHorizontal: 16,
  },
  input: {
    borderWidth: 1,
    paddingVertical: 10,
    paddingHorizontal: 5,
    borderRadius: 6,
    marginBottom: 10,
  },
  contentNews: {
    backgroundColor: "grey",
    paddingVertical: 10,
  },
});
