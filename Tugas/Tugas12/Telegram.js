import React from "react";
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  Touchable,
  TouchableOpacity,
  View,
} from "react-native";
import { Data } from "./dummy";

export default function Telegram() {
  useEffect(() => {
    console.log(Data);
  }, []);
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={styles.subHeader}>
          <View style={styles.subContentHeader}>
            <Image style={styles.avatarDrawer} />
          </View>
        </View>
      </View>
      <View style={style.content}>
        <SafeAreaView>
          <FlatList
            data={Data}
            keyExtractor={(item) => item.id}
            renderItem={({ item }) => {
              return (
                <>
                  <TouchableOpacity style={styles.messageContainer}>
                    <View style={{ flexDirection: "row" }}>
                      <Image
                        styles={{ height: 50, width: 50, borderRadius: 50 }}
                      />
                      <View styles={style.name.Message}>
                        <Text>{item.name}</Text>
                        <Text>{item.message}</Text>
                      </View>
                    </View>
                    <View styles={{ alignItem: "center" }}>
                      <View
                        styles={{ flexDirection: "row", alignItem: "center" }}
                      >
                        <Image
                          style={{ height: 8, width: 12, marginRight: 5 }}
                          source={require("./asset/check.png")}
                        />
                        <Text>{item.time}</Text>
                      </View>
                      <View
                        styles={{
                          backgroundColor: "#4ECC5E",
                          height: 24,
                          weidth: 24,
                          borderRadius: 12,
                          alignItem: "center",
                        }}
                      >
                        <Text styles={{ color: "white" }}>
                          {item.totalMessage}
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                  <View
                    styles={{
                      borderBottomWidth: 1,
                      borderBottomColor: "#A8AAAB",
                    }}
                  />
                </>
              );
            }}
          />
        </SafeAreaView>
        <TouchableOpacity style={{ paddingRight: 24, marginTop: 20 }}>
          <View styles={style.addChat}>
            <Image
              styles={style.avatarPen}
              source={require("./asset/pencil.png")}
            />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const style = StyleShet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header:{
    height: 50,
    backgroundColor:'#517DA2',
    marginTop:30,
    justifyContent:'center'
  },
  subHeader:{
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 18,
    justifyContent:'center'
  },
  avatarDrawer:{
    height: 12,
    width: 18
  },
  titleName:{
    color:'white',
    fontSize: 18,
    paddingLeft: 20
  },
  messageContainer:{
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingTop: 27,
    paddingHorizontal: 24,
    paddingVertical: 24,
    justifyContent:'space-between',

  },
  nameMessage:{
    paddingLeft: 15
  },
  avatarPen:{
    height:18,
    width:18
  },
  addChat:{
    height: 45,
    width: 45,
    borderRadius: 45 / 2,
    backgroundColor: '4EA4EA',
    justifyContent:'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
  }
})

const styles = StyleSheet.create({});
