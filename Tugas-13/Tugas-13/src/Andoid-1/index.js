import React from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  ImageBackground,
} from "react-native";

export default function Android1() {
  return (
    <View>
      <View style={{ alignSelf: "center", marginTop: 70 }}>
        <Text style={{ color: "#003366", fontSize: 20 }}>Tentang Saya</Text>
      </View>
      <Image source={require("../asset/gambar.jpg")} style={styles.gambar} />
      <View style={{ alignSelf: "center", marginTop: 20 }}>
        <Text style={{ color: "#003366", fontSize: 20 }}>Galuh Raka Siwa</Text>
      </View>
      <View style={{ alignSelf: "center", marginTop: 20 }}>
        <Text style={{ color: "#003366", fontSize: 20, paddingBottom:20 }}>
          React Native Developer
        </Text>
      </View>
      <View style={{ backgroundColor: "#EFEFEF", height: 160 }}>
        <View
          style={{
            paddingLeft: 30,
            marginTop: 40,
            borderBottomWidth: 1,
            backgroundColor: "#EFEFEF",
          }}
        >
          <Text style={{ color: "#003366", fontSize: 20 }}>Portofolio</Text>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            paddingLeft: 100,
            paddingRight: 120,
            marginTop: 10,

          }}
        >
          <Image
            source={require("../asset/Vector.png")}
            style={styles.Register}
          />

          <Image
            source={require("../asset/Vector(1).png")}
            style={styles.Register}
          />
        </View>

        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            paddingLeft: 80,
            paddingRight: 90,
            marginTop: 10,
          }}
        >
          <Text style={{ color: "black", fontSize: 12 }}>@galuhrakasiwa</Text>
          <Text style={{ color: "black", fontSize: 12 }}>@galuhrakasiwa</Text>
        </View>

        <View style= {{paddingTop:20}}></View>
        <View style={{ backgroundColor: "#EFEFEF", height: 160 }}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            paddingLeft: 50,
            paddingRight: 90,
            marginTop: 10,
          }}
        >
        
        <Text style={{ color: "#003366", fontSize: 20 }}>Contact</Text>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            paddingLeft: 100,
            paddingRight: 120,
            marginTop: 10,
          }}
        >
          <Image
            source={require("../asset/Vector(3).png")}
            style={styles.Register}
          />

          <Image
            source={require("../asset/Vector(2).png")}
            style={styles.Register}
          />
        </View>

        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            paddingLeft: 80,
            paddingRight: 90,
            marginTop: 10,
          }}
        >
          <Text style={{ color: "black", fontSize: 12 }}>@galuhrakasiwa</Text>
          <Text style={{ color: "black", fontSize: 12 }}>@galuhrakasiwa</Text>
          </View>
        </View>
      </View>
    </View>
  );
}

// css

const styles = StyleSheet.create({
  gambar: {
    marginTop: 50,
    alignSelf: "center",
  },
});
